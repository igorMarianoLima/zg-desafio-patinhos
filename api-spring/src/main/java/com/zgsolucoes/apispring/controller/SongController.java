package com.zgsolucoes.apispring.controller;

import com.zgsolucoes.apispring.dto.ResponseDto;
import com.zgsolucoes.apispring.model.Animal;
import com.zgsolucoes.apispring.model.Place;
import com.zgsolucoes.apispring.model.Song;
import com.zgsolucoes.apispring.repository.AnimalRepository;
import com.zgsolucoes.apispring.repository.PlaceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/song")
public class SongController {
    AnimalRepository animalRepository;
    PlaceRepository placeRepository;

    public SongController()
    {
        this.animalRepository = new AnimalRepository();
        this.animalRepository.add(new Animal("patinha", "patinho", "patinhos", "Quá, quá, quá, quá"));
        this.animalRepository.add(new Animal("gatinha", "gatinho", "gatinhos", "Miau, miau, miau, miau"));

        this.placeRepository = new PlaceRepository();
        this.placeRepository.add(new Place("montanha", "Além das montanhas"));
        this.placeRepository.add(new Place("lago", "Além dos lagos"));
        this.placeRepository.add(new Place("parque", "Além dos parques"));
    }

    @GetMapping("/ducks/animals")
    public ResponseDto<String[]> getAnimals()
    {
        List<String> names = new ArrayList<>();
        this.animalRepository.get().forEach(animal -> names.add(animal.singular));

        return new ResponseDto<String[]>(
                "Animais disponíveis",
                names.toArray(new String[]{})
        );
    }

    @GetMapping("/ducks/places")
    public ResponseDto<String[]> getPlaces()
    {
        List<String> names = new ArrayList<>();
        this.placeRepository.get().forEach(place -> names.add(place.name));

        return new ResponseDto<String[]>(
                "Locais disponíveis",
                names.toArray(new String[]{})
        );
    }

    @GetMapping("/ducks/{quantity}")
    public ResponseDto<Song> getDucks(
            @PathVariable("quantity") int quantity,
            @RequestParam(value = "character", defaultValue = "patinho") String animalName,
            @RequestParam(value = "where", defaultValue = "montanha") String placeName
    ) {
        Optional<Animal> animalFound = this.animalRepository.find(animalName).findFirst();
        Optional<Place> placeFound = this.placeRepository.find(placeName).findFirst();

        if (animalFound.isEmpty() || placeFound.isEmpty() || quantity < 1)
        {
            throw new ErrorResponseException(HttpStatus.BAD_REQUEST);
        }

        Animal animal = animalFound.get();
        Place place = placeFound.get();

        String songTitle = String.format(
                "%d %s",
                quantity,
                quantity > 1 ? animal.plural : animal.singular
        );

        List<String> verses = new ArrayList<String>();
        List<String[]> paragraphs = new ArrayList<String[]>();
        for (int actualAnimalQuantity = quantity; actualAnimalQuantity > 0; actualAnimalQuantity--)
        {
            int quantityAnimalsCameBack = actualAnimalQuantity - 1;

            verses.add(
                    String.format("%d %s", actualAnimalQuantity, actualAnimalQuantity > 1 ? animal.plural : animal.singular)
            );
            verses.add(
                    String.format("%s passear", actualAnimalQuantity > 1 ? "foram" : "foi")
            );
            verses.add(place.phrase);
            verses.add("Para brincar");
            verses.add("A mamãe gritou");
            verses.add(animal.sound);

            if (quantityAnimalsCameBack == 0) {
                verses.add(
                        String.format("Mas nenhum %s voltou de lá", animal.singular)
                );
            } else {
                verses.add(
                        String.format("Mas só %d %s", quantityAnimalsCameBack, quantityAnimalsCameBack > 1 ? animal.plural : animal.singular)
                );
                verses.add("voltaram de lá");
            }

            String[] paragraph = new String[verses.size()];
            paragraphs.add(verses.toArray(paragraph));

            verses.clear();
        }

        verses.add(
                String.format("A mamãe %s foi procurar", animal.mother)
        );
        verses.add("Na beira do mar");
        verses.add("A mamãe gritou");
        verses.add(animal.sound);
        verses.add(
            String.format("E %s %d %s", quantity > 1 ? "os" : "", quantity, quantity > 1 ? animal.plural : animal.singular)
        );
        verses.add(
                String.format("%s de lá", quantity > 1 ? "voltaram" : "voltou")
        );

        paragraphs.add(verses.toArray(new String[6]));
        verses.clear();

        Song song = new Song(songTitle, paragraphs.toArray(new String[quantity][8]));

        return new ResponseDto<Song>(
                "Letra gerada com sucesso",
                song
        );
    }
}
