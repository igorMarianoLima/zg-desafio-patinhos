package com.zgsolucoes.apispring.dto;

public class ResponseDto<T> {
    public String message;
    public T data;

    public ResponseDto(
            String message,
            T data
    )
    {
        this.message = message;
        this.data = data;
    }
}
