package com.zgsolucoes.apispring.model;

public class Animal {
    public String mother;
    public String singular;
    public String plural;
    public String sound;

    public Animal(
            String mother,
            String singular,
            String plural,
            String sound
    )
    {
        this.mother = mother;
        this.singular = singular;
        this.plural = plural;
        this.sound = sound;
    }
}
