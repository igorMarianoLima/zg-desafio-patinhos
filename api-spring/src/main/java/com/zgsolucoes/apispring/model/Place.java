package com.zgsolucoes.apispring.model;

public class Place {
    public String name;
    public String phrase;

    public Place(
            String name,
            String phrase
    )
    {
        this.name = name;
        this.phrase = phrase;
    }
}
