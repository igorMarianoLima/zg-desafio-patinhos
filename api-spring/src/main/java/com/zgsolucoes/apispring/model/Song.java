package com.zgsolucoes.apispring.model;

public record Song(String title, String[][] paragraphs) {}
