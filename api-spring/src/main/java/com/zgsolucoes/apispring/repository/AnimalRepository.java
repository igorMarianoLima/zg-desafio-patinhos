package com.zgsolucoes.apispring.repository;

import com.zgsolucoes.apispring.model.Animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class AnimalRepository {
    List<Animal> animals = new ArrayList<>();

    public List<Animal> get()
    {
        return this.animals;
    }

    public void add(Animal animal)
    {
        this.animals.add(animal);
    }

    public void remove(String animalName)
    {
        this.animals.removeIf(animal -> animal.singular.equals(animalName));
    }

    public Stream<Animal> find(String animalName)
    {
        return this.animals.stream().filter(animal -> animal.singular.equals(animalName));
    }

    public void update(String animalName, Animal newAnimal)
    {
        Optional<Animal> animal = this.animals.stream().filter(a -> a.singular.equals(animalName)).findFirst();

        if (animal.isEmpty()) return;

        int index = this.animals.indexOf(animal.get());
        this.animals.set(index, newAnimal);
    }
}
