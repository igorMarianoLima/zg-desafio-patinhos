package com.zgsolucoes.apispring.repository;

import com.zgsolucoes.apispring.model.Place;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class PlaceRepository {
    List<Place> places = new ArrayList<>();

    public List<Place> get()
    {
        return this.places;
    }

    public void add(Place place)
    {
        this.places.add(place);
    }

    public void remove(String placeName)
    {
        this.places.removeIf(place -> place.name.equals(placeName));
    }

    public Stream<Place> find(String placeName)
    {
        return this.places.stream().filter(place -> place.name.equals(placeName));
    }

    public void update(String placeName, Place newPlace)
    {
        Optional<Place> place = this.places.stream().filter(p -> p.name.equals(placeName)).findFirst();

        if (place.isEmpty()) return;

        int index = this.places.indexOf(place.get());
        this.places.set(index, newPlace);
    }
}
