import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'song',
    loadChildren: () => import('./pages/song/song.module').then(m => m.SongModule)
  },
  {
    path: '**',
    redirectTo: 'song'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
