import { Component } from '@angular/core';

import { ApiSongService } from 'src/app/shared/services/apiSong/api-song.service';
import { Song } from 'src/app/shared/interfaces/services/apiSong/response';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})
export class SongComponent {
  song: Song;
  animalQuantity = '';

  animals: string[] = [];
  animal: string = this.animals[0]

  places: string[] = []
  place: string = this.places[0];

  constructor(
    private apiSongService: ApiSongService
  ) {
    this.song = {} as Song;
  }

  ngOnInit() {
    this.fetchAnimals();
    this.fetchPlaces();
  }

  fetchAnimals() {
    this.apiSongService.getAnimals().subscribe({
      next: res => {
        this.animals = res.data;
        this.animal = this.animals[0];
      }
    })
  }

  fetchPlaces() {
    this.apiSongService.getPlaces().subscribe({
      next: res => {
        this.places = res.data;
        this.place = this.places[0];
      }
    })
  }

  handleAnimalName(animalValue: string) {
    this.animal = this.animals.find(a => a === animalValue)!;
  }

  handlePlaceName(placeValue: string) {
    this.place = this.places.find(p => p === placeValue)!;
  }

  isQuantityValid() {
    const quantity = parseInt(this.animalQuantity);
    return !isNaN(quantity) && quantity >= 1;
  }

  fetchLyrics() {
    const quantity = parseInt(this.animalQuantity);

    if (!this.isQuantityValid()) return alert('Insira um número inteiro positivo');

    this.apiSongService.getSongLyrics({
      animalsQuantity: quantity,
      character: this.animal,
      where: this.place
    }).subscribe({
      next: res => this.song = res.data
    })
  }
}
