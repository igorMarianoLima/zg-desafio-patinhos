import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SongRoutingModule } from './song-routing.module';
import { SongComponent } from './song.component';

import { CoreModule } from 'src/app/core/core.module';
import { InputModule } from 'src/app/shared/components/input/input.module';
import { ButtonModule } from 'src/app/shared/components/button/button.module';


@NgModule({
  declarations: [
    SongComponent
  ],
  imports: [
    CommonModule,
    SongRoutingModule,
    CoreModule,
    InputModule,
    ButtonModule
  ]
})
export class SongModule { }
