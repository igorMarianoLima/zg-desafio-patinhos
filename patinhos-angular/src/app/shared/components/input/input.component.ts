import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  @Input() value = '';

  @Output() valueChange = new EventEmitter<string>();

  handleInputChange(event: Event) {
    const target = (event.target) as HTMLInputElement;

    this.valueChange.emit(target.value);
  }
}
