export interface SongRequest {
    animalsQuantity: number;
    character: string;
    where: string;
}