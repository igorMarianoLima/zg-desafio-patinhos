export interface PlacesResponse {
    message: string;
    data: string[];
}

export interface AnimalsResponse {
    message: string;
    data: string[];
}

export interface SongResponse {
    message: string;
    data: Song;
}

export interface Song {
    title: string;
    paragraphs: string[][];
}