import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment.development';

import { SongRequest } from '../../interfaces/services/apiSong/request';
import { AnimalsResponse, PlacesResponse, SongResponse } from '../../interfaces/services/apiSong/response';

@Injectable({
  providedIn: 'root'
})
export class ApiSongService {
  private readonly API_BASE_URL = environment.SONG_API_BASE_URL;

  constructor(
    private http: HttpClient
  ) { }

  getAnimals() {
    return this.http.get<AnimalsResponse>(`${this.API_BASE_URL}/song/ducks/animals`)
  }

  getPlaces() {
    return this.http.get<PlacesResponse>(`${this.API_BASE_URL}/song/ducks/places`)
  }

  getSongLyrics({ animalsQuantity, ...params }: SongRequest) {
    return this.http.get<SongResponse>(`${this.API_BASE_URL}/song/ducks/${animalsQuantity}`, {
      params
    })
  }
}
